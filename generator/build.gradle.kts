dependencies {
    implementation(project(":core", "default"))

    implementation("org.json:json:20231013")
    implementation("commons-io:commons-io:2.11.0")
    implementation("com.google.guava:guava:33.1.0-jre")
}
